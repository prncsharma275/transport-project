<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BillRegister extends Model
{
    protected $table = 'bill_register';
}
