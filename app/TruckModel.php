<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TruckModel extends Model
{
    protected $table = "truckmodels";
}
