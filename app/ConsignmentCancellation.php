<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ConsignmentCancellation extends Model
{
    protected $table = 'consignment_cancellation';
}
