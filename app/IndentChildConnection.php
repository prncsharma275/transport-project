<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IndentChildConnection extends Model
{
    protected $table = 'indent_child_connection';
}
