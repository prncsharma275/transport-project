<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class SingleViewPanelController extends Controller
{
    public  function single_consignment_view_panel(){

        return View::make('ViewPanel.Coke Consignment View.single_coke_consignment_view_entry');
    }
    public  function coke_consignment_view_report_panel(Request $request){

        $consignment_no=$request->consignment_no;
        return View::make('ViewPanel.Coke Consignment View.coke_consignment_view_report')->with('consignment_no',$consignment_no);
    }
    public  function single_consignment_print_panel(){

        return View::make('SinglePrintPanel.Coke Consignment View.single_coke_consignment_view_entry');
    }
    public  function coke_consignment_print_report_panel(Request $request){

        $consignment_no=$request->consignment_no;
        return View::make('SinglePrintPanel.Coke Consignment View.coke_consignment_view_report')->with('consignment_no',$consignment_no);
    }


}
