<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IndentPlacement extends Model
{
    protected $table = 'indent_placement';
}
