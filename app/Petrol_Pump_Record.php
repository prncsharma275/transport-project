<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Petrol_Pump_Record extends Model
{
    protected $table = 'petrol_pump_record';
}
