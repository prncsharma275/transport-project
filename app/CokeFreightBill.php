<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CokeFreightBill extends Model
{
    protected $table = 'coke_freight_bill';
}
