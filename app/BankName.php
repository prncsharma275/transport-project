<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BankName extends Model
{
    protected $table = 'bank_name';
}
