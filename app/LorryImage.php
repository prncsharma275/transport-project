<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LorryImage extends Model
{
    protected $table = 'lorry_image';
}
