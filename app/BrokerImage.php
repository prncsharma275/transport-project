<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BrokerImage extends Model
{
    protected $table = 'broker_image';
}
