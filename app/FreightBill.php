<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FreightBill extends Model
{
    protected $table = 'freight_bill';
}
