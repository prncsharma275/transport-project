@extends('layouts.adminPanelTable')
@section('title')
   New General Notice
@endsection

@section('custom_css')
    <link rel="stylesheet" href="{{ asset('style/css/jquery-ui.css')}}">
    <script src="{{ asset('style/js/jquery.min.js')}}"></script>
    <script src="{{ asset('style/js/jquery-ui.js')}}"></script>
@endsection

@section('manual_style_code')
    <style>
        table tr, td, th{
            color: #000!important;
            padding: 5px!important;
            text-align: center;

        }
        tr, td.border_bottom td {
            border-bottom:1pt solid black !important;
        }
        input[type='search']{
            width: 200px!important;
        }

        .well{
            background-color: #ffffff;
        }
        select{
            padding: 0;
            color: #000000;
        }
    </style>
    <script type="text/javascript">
        $(function() {
            $( ".datepicker" ).datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'dd/mm/yy'
            });
        });
    </script>
@endsection

@section('content')


    <h4 class="page-header text-center">New General Notice</h4>
    <?php echo Form::open(array('route' => 'store_general_notice','files' => true, 'method' => 'post')); ?>

    <div class="row">
        <div class="col-md-12 form-group">
            <div class=" has-success has-feedback">
                <label>Title Of The Notice<span style="color: red; font-size: 15px">*</span></label>
                <div class="input-group">
                    <input  type="text" class="form-control" name="title" placeholder="Title">

                </div>
            </div>
        </div>
        <div class="col-md-12 form-group">
            <div class=" has-success has-feedback">
                <label>Description<span style="font-size: 12px;color: red">*</span></label>
                <div class="input-group">
                    <textarea class="form-control" name="description" style="height: 300px;resize: none"></textarea>
                </div>
            </div>
        </div>

        <div class="col-md-4 form-group">
            <div class=" has-success has-feedback">
                <label>Issue Date<span style="font-size: 12px;color: red">*</span></label>
                <div class="input-group">
                    <input  type="text" class="form-control datepicker" name="issue_date" placeholder="Issue Date">
                </div>
            </div>
        </div>



        <div class="col-md-4 form-group">
            <div class=" has-success has-feedback">
                <label>Expiry Date <span style="font-size: 12px;color: red">*</span></label>
                <div class="input-group">
                    <input  type="text" class="form-control datepicker" name="expiry_date" placeholder="Expiry Date">

                </div>
            </div>
        </div>

        <div class="col-md-4 form-group">
            <div class=" has-success has-feedback">
                <label>PDF Upload <span style="font-size: 11px;color: green">(If Any)</span></label>
                <div class="input-group">
                    <input  type="file" class="form-control" name="pdf" placeholder="Pdf">
                </div>
            </div>
        </div>






</div>


    <br>
    <div class="form-group">
        <label class="col-md-4 control-label"></label>
        <div class="col-md-4">
            <button type="submit" class="btn btn-warning" >Save <span class="glyphicon glyphicon-hdd"></span></button>

            <button type="reset" class="btn btn-info" >Reset<span class="glyphicon glyphicon-refresh"></span></button>
            <a class="btn btn-success" href="{{url('customers')}}">Back</a>
        </div>
    </div>
    {{--</Form>--}}
    {{form::close()}}

    @if($errors->any())
        <div class="alert alert-danger">
            @foreach($errors->all() as $error)
                <p>{{ $error }}</p>
            @endforeach
        </div>
    @endif

    <br><br>
@endsection