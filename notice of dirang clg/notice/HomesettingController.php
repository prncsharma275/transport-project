<?php

namespace App\Http\Controllers;

use App\Homeslider;
use App\Merit_gen;
use App\Notice_addmission;
use App\Notice_examination;
use App\Notice_general;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
//use Illuminate\Support\Facades\save;
use Illuminate\Support\Facades\Input;
//use Illuminate\Support\Facades\File;
use App\Merit_hon;
use Monolog\Handler\NullHandlerTest;
use Session;
use File;
use Illuminate\Support\Facades\DB;


class HomesettingController extends Controller
{

    public function home_page_setting()
    {

        return View::make('home_setting/panel_page');
    }


//    home slider coding start here

    public function home_slider()
    {
        $image = Homeslider::all();
        return View::make('home_setting/homeslider')->with('image', $image);
    }

    public function updateHomeslider(Request $request, $id)
    {
        $this->validate($request, [

            'image' => 'required|image|mimes:jpeg,jpg,png|max:2000|dimensions:min_width=1320,min_height=495',

        ]);

        $oldfile = Homeslider::find($id);
//        print_r($oldfile);
//        die();
        $file = Input::file('image');
//        print_r($file);
//        die();

//            foreach ($files as $file) {
//                print_r($file);
//                die();
        $milliseconds = round(microtime(true) * 10);
        $filename =  $milliseconds . $file->getClientOriginalName();
//                echo $filename;
//                die();
//              $file->move('uploads','3'. $file->getClientOriginalName());
        $file->move('homefolder/slider', $filename);
        $flight = Homeslider::find($id);
        $flight->sliderName = $filename;

//                 $path= base_path('uploads/'.$oldfile->productImage);
//                print_r($path);
//                die();
        $file_path = base_path("homefolder/slider/{$oldfile->sliderName}");

        if (File::exists($file_path)) File::delete($file_path);


        $flight->save();
//            }

        Session::flash('flash_message', 'images successfully updates!');
        return redirect()->route('home_slider');

    }

    public function firstBanner(){
        $bannerFirst =  FirstBanner::all();
        return View::make('FrontendPage/firstBanner')->with('bannerFirst', $bannerFirst);
    }

    public function general_notice(){
        $all_general_notice = Notice_general::all();
        return View::make('home_setting/general_notice')->with('general_notice',$all_general_notice);
    }

  public function new_general_notice(){
      return View::make('home_setting/new_general_notice');

  }


    public function store_general_notice(Request $request){

//        $mydata= Input::all();
//        print_r($mydata);
//        die();
        $billing_date = explode('/', $request->input('expiry_date'));
        $billing_day = $billing_date[0];
        $billing_month = $billing_date[1];
        $billing_year = $billing_date[2];

        $data = New Notice_general();
        $data->notice_title=$request->title;
        $data->notice_description=$request->description;
        $data->issue_date=$request->issue_date;
        $data->expiry_date=date($billing_year . '-' . $billing_month . '-' . $billing_day);


        $file = Input::file('pdf');
        if(!empty($file)){
            $milliseconds = round(microtime(true) * 10);
            $filename =  $milliseconds . $file->getClientOriginalName();
            $file->move('homefolder/document', $filename);
            $data->pdf_path = $filename;
        }
        $data->save();
         return redirect()->route('general_notice');
    }


//    Addmission notice

    public function addmission_notice(){

        $all_general_notice = Notice_addmission::all();
        return View::make('home_setting/addmission_notice')->with('general_notice',$all_general_notice);
    }

    public function new_addmission_notice(){
        return View::make('home_setting/new_addmission_notice');

    }


    public function store_addmission_notice(Request $request){

//        $mydata= Input::all();
//        print_r($mydata);
//        die();
        $billing_date = explode('/', $request->input('expiry_date'));
        $billing_day = $billing_date[0];
        $billing_month = $billing_date[1];
        $billing_year = $billing_date[2];

        $data = New Notice_addmission();
        $data->notice_title=$request->title;
        $data->notice_description=$request->description;
        $data->issue_date=$request->issue_date;
        $data->expiry_date=date($billing_year . '-' . $billing_month . '-' . $billing_day);


        $file = Input::file('pdf');
        if(!empty($file)){
            $milliseconds = round(microtime(true) * 10);
            $filename =  $milliseconds . $file->getClientOriginalName();
            $file->move('homefolder/document', $filename);
            $data->pdf_path = $filename;
        }
        $data->save();
        return redirect()->route('addmission_notice');
    }





    //    Examination notice

    public function examination_notice(){

        $all_general_notice = Notice_examination::all();
        return View::make('home_setting/examination_notice')->with('general_notice',$all_general_notice);
    }

    public function new_examination_notice(){
        return View::make('home_setting/new_examination_notice');

    }


    public function store_examination_notice(Request $request){

//        $mydata= Input::all();
//        print_r($mydata);
//        die();
        $billing_date = explode('/', $request->input('expiry_date'));
        $billing_day = $billing_date[0];
        $billing_month = $billing_date[1];
        $billing_year = $billing_date[2];

        $data = New Notice_examination();
        $data->notice_title=$request->title;
        $data->notice_description=$request->description;
        $data->issue_date=$request->issue_date;
        $data->expiry_date=date($billing_year . '-' . $billing_month . '-' . $billing_day);


        $file = Input::file('pdf');
        if(!empty($file)){
            $milliseconds = round(microtime(true) * 10);
            $filename =  $milliseconds . $file->getClientOriginalName();
            $file->move('homefolder/document', $filename);
            $data->pdf_path = $filename;
        }

        $data->save();
        return redirect()->route('examination_notice');
    }



}
