@extends('layouts.adminPanelTable')
@section('title')
    Addmission Notice
@endsection

@section('custom_css')

@endsection

@section('manual_style_code')
    <style>
        table tr, td, th{
            color: #000!important;
            padding: 5px!important;
            text-align: center;

        }
        tr, td.border_bottom td {
            border-bottom:1pt solid black !important;
        }
        input[type='search']{
            width: 200px!important;
        }

        .well{
            background-color: #ffffff;
        }
        .table-striped > tbody > tr:nth-child(2n+1) > td, .table-striped > tbody > tr:nth-child(2n+1) > th {
            background-color:  #e1f8ff;
        }
    </style>
@endsection

@section('content')


    <h4 class="text-center">Addmission Notice</h4>
    <a class="btn btn-warning" style="margin-bottom: 10px;background-color: #EB3E28 " href="{{url('new_addmission_notice')}}">+ New Notice</a>
    <a class="btn btn-warning" style="margin-bottom: 10px" href="{{url('home_page_setting')}}">Back</a>

    @if(Session::has('danger'))
        <div class="alert alert-danger" style="font-size: 16px;color: #000">
            {{ Session::get('danger') }}
        </div>
    @endif

    <table  class="table-striped table-bordered" cellspacing="0" style="width: 100%; border-collapse: collapse" id="table">
        <thead>
        <tr>

            <th class="text-center">Title</th>

            <th class="text-center">Description</th>
            <th class="text-center">Issue Date</th>
            <th class="text-center">Expiry Date</th>
            <th class="text-center">Attachment</th>

            <th class="text-center">Actions</th>
        </tr>
        </thead>
        <tbody>
        @foreach($general_notice as $item)

            <tr>
                <td>{{$item->notice_title}}</td>
                <td>{{str_limit($item->notice_description,30)}}</td>
                <td>{{$item->issue_date}}</td>
                <td>{{$item->expiry_date}}</td>

                @if($item->pdf_path=="")
                <td>----</td>
                 @else
                    <td><i class="fa fa-file-pdf-o" style="font-size:24px;color:red"></i></td>
                    @endif
                <td>
                    <a href="#" class="btn btn-success btn-sm">
                        View
                    </a>
                    <a href="#" class="btn btn-info btn-sm">
                        Edit
                    </a>
                    <a href="#" class="btn btn-danger btn-sm" onclick="return ConfirmDelete()">
                        Delete
                    </a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    @if(Session::has('flash_message'))
        <div class="alert alert-success">
            {{ Session::get('flash_message') }}
        </div>
    @endif
    <script>
        $(document).ready(function() {
            $('#table').DataTable();
        } );

        //    delete commande
        function ConfirmDelete()
        {
            var x = confirm("Are you sure you want to Cancel This Bill?");
            if (x)
                return true;
            else
                return false;
        }


    </script>
    {{--inner content here ------------------------------------}}

    <br>
@endsection