<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <!--[if IE]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <![endif]-->
    <title>$challan->challan_no}}</title>
    <!-- BOOTSTRAP CORE STYLE  -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <!-- GOOGLE FONTS -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,300' rel='stylesheet' type='text/css' />
    <style>
        body {
            padding: 0px;
        }
        @page {
            margin-top: 0.3em;
            margin-left: 2em;
            margin-bottom:0.3em;
        }
        #content-table tr td{
            padding-left: 5px;
            padding-bottom: 5px;
            padding-right: 2px;
            padding-top: 5px;
            border: none;
        }
        #contains-table thead tr th{
            border: 1px solid black;
        }
        #content-table1 tr td{
            padding: 3px;border: none;
        }
        #content-table2 tr td{
            padding: 5px;
        }
        #dynamic_tbody tr td{
            padding: 10px;
        }
        #amount_table{
            text-align: right;
        }
        #heading-down-table tr td{
            padding: 3px;
            border: none;
        }
        #contains-table tbody tr td{
            padding: 2px;border: 1px solid black;
        }
        #amount_table tr td{
            border: 1px solid black;
        }
        .boxex{
            display: flex;
            flex-flow: row nowrap;
        }
    </style>
</head>
<body>
<div class="lorry-advance" style="margin-top: 5px;padding: 2px;">

    <div class="col-lg-12 col-md-12 col-sm-12 text-center"style="padding-bottom: -15px;">
        <img src="asset('/images/logo.jpg')}}" width="50" height="50" style="float: left;margin-top: 20px;margin-left: 10px;"/>
        <p style="font-size: 13px;width: fit-content;margin-top: 10px;text-decoration: underline;margin-bottom: -7px;">BALANCE</p>
        <p style="font-size: 25px;margin-bottom: -25px;">Shri Bhawani Transport Services</p><br>
        <p style="width: content-box;font-size: 13px;margin-bottom: -2px;">Sarkar Para, Bankimnagar, Sevoke Road, Siliguri - 734001</p>
        <p style="font-size: 13px;">+91-9832617171, +91-9475535150 www.sbts.co.in / e-mail - naveen@sbts.co.in</p>
        <p style="font-size: 9px;margin-top: 20px;">INVOICE</p>
    </div>
    <div class="col-lg-12 col-md-12 col-sm-12 text-left"style="padding-bottom: -15px;">
        <p style="font-size: 13px;">+91-9832617171, +91-9475535150 www.sbts.co.in / e-mail - naveen@sbts.co.in</p>
    </div>
</div>


</body>
</html>
